import { Component, OnInit,Input} from '@angular/core';
import{PreregistroService} from '../servicios/preregistro.service';
import { first } from 'rxjs/operators';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {

  listausuarios=[];
  constructor(private getusers:PreregistroService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getallusers();
  }
  getallusers(){
    this.getusers.getallpre().subscribe(
     (data)=>{

      console.log('Data',data);
      
      this.listausuarios=data['data'];

     },
     (error)=>{
       console.error('Fallo en la comunicacion')
     }
   );
  }

  createuser(postin){

    var sa = postin;
    console.log(sa);
    console.log(sa.rol);
    this.getusers.getrol(sa.rol).pipe(first())
    .subscribe(
        data => {         
          var datas = data[0];
          sa.rol = datas.idRoles
         
          this.getusers.createuser(sa)
        .pipe(first())
        .subscribe(
            data => {
              console.log(data);
              this.getusers.deleteuser(postin.idPreUsuario).subscribe();
              alert("Generado, usuario creado activo en el sistema");
              this.reloadPage();
            },
            error => {
              console.log(error);
              alert("Error, No se puede crear el usuario en el sistema");
              this.reloadPage();
            });
        },
        error => {
          console.log(error);
          alert("Error, No se puede crear el usuario en el sistema rol sin validez")
          this.reloadPage();
        });
    
    
    //    this.getallusers();
    //    this.reloadPage();
    
  }
  reloadPage() {
    window.location.reload();
}

  deleteuser(iduser){
    this.getusers.deleteuser(iduser).subscribe(
      
    );
    alert("Usuario eliminado");
    this.getallusers();
    this.reloadPage();
  }

}
