import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import{usuario} from '../Modils/Usuario.model';
import {afiche} from '../Modils/Afiche.models';

import {PosterService} from '../servicios/poster.service';
import {UsuarioServices} from '../servicios/Usuario.service';

@Component({
  selector: 'app-poster-list',
  templateUrl: './poster-list.component.html',
  styleUrls: ['./poster-list.component.css']
})

export class PosterListComponent implements OnInit {

  afiche: afiche; 
  listaimagenes: afiche[];
  usuario: usuario;
  

  constructor(private posterServices: PosterService, private usuarioServices: UsuarioServices) {
    this.afiche = new afiche;
    this.usuario = new usuario;
    this.listaimagenes=new Array<afiche>();
   }

  ngOnInit(): void {
    this.posterServices.getposters().subscribe(afiches => {
      this.listaimagenes = afiches as Array<afiche>;
    });
  }
  //this.posterServices.getposters().subscribe(res =>{this.afiche.titulo = res;} , err =>console.error(err))

 
  deletePoster(afiche: afiche): void {
    this.posterServices.deleteposter(afiche.IdAfiche )
      .subscribe( data => {
        //this.afiche = this.afiche.filter(u => u !== afiche);
      })

  
  };
  updateProduct(ProductName, ProductDescription, ProductPrice, id) {
    
  }

  



}
