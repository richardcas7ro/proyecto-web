import { Component, OnInit } from '@angular/core';
import {usuario} from '../Modils/Usuario.model';
import { Login } from '../servicios/login.service';
import { RandomService } from '../servicios/random.service';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder,  Validators } from '@angular/forms';
import { CommonModule }  from '@angular/common';
import { first } from 'rxjs/operators';
import { NgModule }      from '@angular/core';
import { FormGroup , FormControl , ReactiveFormsModule , FormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm2: FormGroup;
  usuarios: usuario;
  logUs:usuario;
  listaUsuarios=[];
  returnUrl: string;
  submitted = false;
  loading = false;
  editedmsg: string;
    
    
    constructor(private login:Login,
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private someserv: RandomService
      ) {
      this.usuarios = new usuario;
      localStorage.setItem('path', 'login');
    }

    get f() { return this.loginForm2.controls; }

      ngOnInit(): void {
        this.loginForm2 = this.formBuilder.group({
          idUsuario:  ['', Validators.required],
          contrasena: ['', Validators.required],
        });
      
    }

    editthemsg() {
      this.someserv.editMsg(this.editedmsg);
    }

    onSubmit() {
      this.submitted = true;
    
      // stop here if form is invalid
      if (this.loginForm2.invalid) {
          return;
      }
      

      var ss =  this.loginForm2.controls;
      this.loading = true;
      this.login.login(ss.idUsuario.value, ss.contrasena.value)
          .pipe(first())
          .subscribe(
              data => {
                var us:any = data;
                us.contrasena = "private";
                localStorage.setItem('usuario', JSON.stringify(us));
                localStorage.setItem('path', 'home');
                this.router.navigate(['/Posters']);
                //this.editedmsg = 'path';
               // this.editthemsg();
              },
              error => {
                alert("Usuario o contraseña incorrectos")
                  this.loading = false;
              });
    }
   
    navegar(){
      this.router.navigate(['/registro'])
    }
  
}

