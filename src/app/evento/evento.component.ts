import { Component, OnInit } from '@angular/core';
import {usuario} from '../Modils/Usuario.model';
import {evento} from '../Modils/evento.models';
import { Login } from '../servicios/login.service';
import { EventoService } from '../servicios/evento.service';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder,  Validators } from '@angular/forms';
import { CommonModule }  from '@angular/common';
import { first } from 'rxjs/operators';
import { NgModule }      from '@angular/core';
import { FormGroup , FormControl , ReactiveFormsModule , FormsModule } from '@angular/forms';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {
  loginForm2: FormGroup;
  currentUser:usuario;
  com:String;
  usuarios: usuario;
  eventos: evento;
  logUs:usuario;
  listaEventos=[];
  returnUrl: string;
  submitted = false;
  loading = false;
  editedmsg: string;
  constructor(private login:Login,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private EventoService: EventoService) { 
      this.currentUser = JSON.parse(localStorage.getItem('usuario'));
      this.com = localStorage.getItem('path');
    }

  ngOnInit(): void {
    this.usuarios = new usuario;
    localStorage.setItem('path', 'login');
    this.loginForm2 = this.formBuilder.group({
      nombre:  ['', Validators.required],
      descripcion: ['', Validators.required],
      fechaIni: ['', Validators.required],
      fechafin: ['', Validators.required],
    });
  }

  get f() { return this.loginForm2.controls; }

  onSubmit() {
    
    var entrada = this.loginForm2.controls;
    
    const newUserObject = {      
      nombre:entrada.nombre.value,
      descripcion:entrada.descripcion.value,
      fechaIni:entrada.fechaIni.value,
      fechafin:entrada.fechafin.value,
      idAprobador: this.currentUser.idUser
    } 
    
    this.EventoService.geteve(newUserObject.nombre).pipe(first())
    .subscribe(
        data => {         
          console.log("error");
          alert("Error, No se puede crear el evento en el sistema.  Ya existe");
        },
        error => { 
          this.EventoService.createEvent(newUserObject)
          .pipe(first())
          .subscribe(
              data => {
                alert("Generado, Evento creado activo en el sistema");
                this.reloadPage();
              },
              error => {
                console.log(error);
                alert("Error, No se puede crear el evento en el sistema")
              });
          
        });
      }
      
      reloadPage() {
        window.location.reload();
      }

}
