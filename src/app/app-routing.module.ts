import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { homeComponent } from './home/home.component';
import { CargarComponent } from './cargar/cargar.component';
import { LoginComponent } from './login/login.component';
import { RolComponent } from './rol/rol.component';
import { PostersComponent } from './posters/posters.component';
import { RegistroComponent } from './registro/registro.component';
import{AdministradorComponent} from './administrador/administrador.component';
import { RolPerfilComponent } from './rol-perfil/rol-perfil.component';
import { EventoComponent } from './evento/evento.component';


const routes: Routes = [
  {path:'', redirectTo:'/login',pathMatch:'full'},
  {path:'Posters', component: homeComponent},
  {path:'cargar-componente', component:CargarComponent},
  {path:'login',component:LoginComponent},
  {path:'registro',component:RegistroComponent},
  {path:'admin',component:AdministradorComponent},
  {path:'rol',component:RolComponent},
  {path:'rolPerfil',component:RolPerfilComponent},
  {path:'poster',component:PostersComponent},
  {path:'evento',component:EventoComponent}
];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
