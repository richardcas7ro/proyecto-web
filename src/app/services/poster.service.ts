
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http'; 
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {RequestOptions, Request, Headers } from '@angular/http';
import { posters } from '../Modils/posters.models';
import { evento } from '../modils/evento.models';


@Injectable({
  providedIn: 'root'
})
export class PosterService {

  constructor(private http: HttpClient) { }

  createEvent(event): Observable<posters>{

    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams();
                     //   let httpBody = new httpBody().set
                     return this.http.post<posters>('http://localhost:3000/poster/create',
                     event,
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }

  createlink(event){
    console.log("createñ")
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams();
                     //   let httpBody = new httpBody().set
                     return this.http.post('http://localhost:3000/event/createlink',
                     event,
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }

  
  geteve(event): Observable<posters>{
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams()
                         .set('titulo', event);
    return this.http.get<posters>('http://localhost:3000/poster/get',
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }


  getevell(usuario):Observable<posters>{
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams()
                      .set('idUsuario', usuario);
    return this.http.get<posters>('http://localhost:3000/poster/getall',
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }

}
