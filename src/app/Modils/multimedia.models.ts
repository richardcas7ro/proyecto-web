export class multimedia{
    idMultimedia : number;
    nombreMultimedia : string;
    ruta : string;
    formato : string;
    ubicacion : string;
    fechaIni : Date;
    fechafin: Date;
};