
import { Component, OnInit } from '@angular/core';
import {usuario} from '../Modils/Usuario.model';
import { Login } from '../servicios/login.service';
import { RandomService } from '../servicios/random.service';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder,  Validators } from '@angular/forms';
import { CommonModule }  from '@angular/common';
import { first } from 'rxjs/operators';
import { NgModule }      from '@angular/core';
import { FormGroup , FormControl , ReactiveFormsModule , FormsModule } from '@angular/forms';


@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  
  loginForm2: FormGroup;
  usuarios: usuario;
  logUs:usuario;
  listaUsuarios=[];
  returnUrl: string;
  submitted = false;
  loading = false;
  editedmsg: string;
  constructor(private login:Login,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private someserv: RandomService) { }

  ngOnInit(): void {
    this.usuarios = new usuario;
    localStorage.setItem('path', 'login');
    this.loginForm2 = this.formBuilder.group({
      idUsuario:  ['', Validators.required],
      contrasena: ['', Validators.required],
    });
  }

  get f() { return this.loginForm2.controls; }

  onSubmit() {}
  navegar(){}


}

