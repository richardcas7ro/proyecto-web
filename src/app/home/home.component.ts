import { Component, OnInit } from '@angular/core';
import {afiche} from '../Modils/Afiche.models';
import {feria} from '../Modils/Feria.models';
import { Button } from 'protractor';
import {usuario} from '../Modils/Usuario.model';
import { MatMenuModule} from '@angular/material/menu';
import { Router,ActivatedRoute } from '@angular/router';
import { ImageCompService } from '../servicios/image-comp.service';
import { first } from 'rxjs/operators';
import { multimedia } from '../Modils/multimedia.models';

class ImageSnippet {

  pending: boolean = false;
  status: string = 'init'; 
 

  constructor(
   public src: string, public file: File
    ) {}
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class homeComponent implements OnInit {   
    currentUser:usuario;
    com:String;
    fila;
    flechaIzquierda;
    flechaDerecha;
    carru;
    numpos ;
    a=true;
    selectedFile: ImageSnippet;
   listaimagenes:multimedia[] = new Array();

    compro:Boolean;  
  
    constructor(private router: Router,
                private route: ActivatedRoute,
               private ImageCompService: ImageCompService) {  

               
        this.flechaIzquierda = document.getElementById('flecha-izquierda');
        this.flechaDerecha = document.getElementById('flecha-derecha');
        this.compro= true; 
        this.currentUser = JSON.parse(localStorage.getItem('usuario'));
        this.com = localStorage.getItem('path');
        
    }



  //corre el carrusel hacia la derecha
    correrderecha(){
      
      document.querySelector('.contenedor-carrusel').scrollLeft += 500;
      this.numpos =document.querySelectorAll('.poster').length;
      console.log(this.numpos);
    
    }
    //corre el carrusel hacia la izquierda
    correrizquierda(){
      
      document.querySelector('.contenedor-carrusel').scrollLeft -= 500;
      

    }

    colocarindicadores(){
      if(this.a){
      var paginas = Math.ceil(document.querySelectorAll('.poster').length/5);
      for(let i=0;i<paginas;i++){
        var indicador=document.createElement('button');
        document.querySelector('.indicadores').appendChild(indicador);
      }
      this.a=false;
      }
    }


    hover(){
      document.querySelectorAll('.poster').forEach((poster)=>{
        poster.addEventListener('mouseenter',(e)=>{
          var elemento = e.currentTarget;
          setTimeout(()=>{
            document.querySelectorAll('.poster').forEach(poster => poster.classList.remove('.hover'));
            
          },300);
        });
      });

    
    }
    
    
    

    ngOnInit(): void {
      console.log("first");
      this.cargarimagenes();
     
    }
    
    

    cargarimagenes(){
            if(this.compro == true){
              console.log("second");
          this.ImageCompService.getevell().pipe(first()).subscribe(
              data => {  
                
                console.log("first2");
                
                for(var i = 0; i < Object.values(data).length; i++){             
                  var datas: multimedia = data[i];
                  this.listaimagenes.push(datas);
                }
                console.log(Object.values(data).length);
              },
              error => { 
                console.log("error");
                alert("Error, No exite contenido en el sistema.");        
        });
        }
    }


    processFile(imageInput: any) {
    
      const file: File = imageInput.files[0];
      const reader = new FileReader();
      
  
      reader.addEventListener('load', (event: any) => {
  
        this.selectedFile = new ImageSnippet(event.target.result, file);
        this.selectedFile.pending = true;
        
         } 
      );
  
      reader.readAsDataURL(file);
    }
  
    private onSuccess() {
      this.selectedFile.pending = false;
      this.selectedFile.status = 'ok';
    }
    
    private onError() {
      this.selectedFile.pending = false;
      this.selectedFile.status = 'fail';
      this.selectedFile.src = '';
    }

    ///////menu____________>
    cargarP(){this.router.navigate(['/cargar-componente']);localStorage.setItem('path', 'cargar-componente');}
    verP(){this.router.navigate(['/Posters']);localStorage.setItem('path', 'Posters');}
    creaE(){ this.router.navigate(['/evento']);localStorage.setItem('path', 'evento');}
    creaRol(){this.router.navigate(['/rol']);localStorage.setItem('path', 'rol');}
    creaPer(){this.router.navigate(['/rolPerfil']);localStorage.setItem('path', 'rolPerfil');}
    crearP(){this.router.navigate(['/poster']);localStorage.setItem('path', 'poster');}
    Aprobar(){this.router.navigate(['/admin']);localStorage.setItem('path', 'admin');}

}

