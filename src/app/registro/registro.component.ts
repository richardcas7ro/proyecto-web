import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import{preusuario} from '../Modils/PreUsusario.Models';
import{PreregistroService} from '../servicios/preregistro.service';
import { FormBuilder,  Validators } from '@angular/forms';
import { CommonModule }  from '@angular/common';
import { first } from 'rxjs/operators';
import { NgModule }      from '@angular/core';
import { FormGroup , FormControl , ReactiveFormsModule , FormsModule } from '@angular/forms';



@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})


export class RegistroComponent implements OnInit {
  loginForm: FormGroup;
  usuarios:preusuario;
  listaUsuarios=[];
  returnUrl: string;
  submitted = false;
  loading = false;


  constructor(private PreregistroService:PreregistroService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
    ) {

    this.usuarios = new preusuario;
   }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      idUsuario:  ['', Validators.required],
      contrasena: ['', Validators.required],
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      correo: ['', Validators.required],
      rol: ['', Validators.required]
    });
  }


  adduser(){
    this.PreregistroService.postallpre(this.usuarios).subscribe(

    );
    alert("Registrado, si cumples con los requisitos al revisar el verificador aceptar")
    this.router.navigate(['/login'])
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
  
    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }
    this.usuarios.idUsuario = this.f.idUsuario.value;
    this.usuarios.contrasena = this.f.contrasena.value;
    this.usuarios.nombres = this.f.nombres.value;
    this.usuarios.apellidos = this.f.apellidos.value;
    this.usuarios.correo = this.f.correo.value;
    this.usuarios.Rol = this.f.rol.value;
   
    this.loading = true;
    this.PreregistroService.postallpre(this.usuarios)
        .pipe(first())
        .subscribe(
            data => {
              alert("Registrado, si cumples con los requisitos al revisar el verificador aceptar")
              this.router.navigate(['/login'])
            },
            error => {
              console.log(error);
              this.loading = false;
            });
  }

 

}

