import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http'; 
import { posters } from '../Modils/posters.models';
import { multimedia } from '../Modils/multimedia.models';

@Injectable({
  providedIn: 'root'
})
export class ImageCompService {

  
   // Http Options
   httpOptions = {
    
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*',
      'Authorization':'authkey',
      'userid':'1',
    }),
    //withCredentials: true
  }  ;
  

  constructor(private http: HttpClient) {
  }

  public uploadImage(image: File, src,nomb,des,poster:posters): Observable<any> {
   
    var formData = new FormData();
    var nom = image.name;
    var form = image.type;

    var  newUserObject = {      
      nombreUsuario:src
    };
    
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
                         .set('userid', '1');
    let httpParams = new HttpParams()
                        .set('nombreMultimedia', nomb)
                        .set('descMultimedia', des)
                        .set('ubicacion', nom)
                        .set('formato', form)
                        .set('fechafin', poster.fechafin + "")
                        .set('fechaIni', poster.fechaIni + "");
    return this.http.post('http://localhost:3000/img/createimg', 
    (newUserObject), {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
    
  }

  getevell():Observable<multimedia>{
    console.log("servicio ");
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
                         .set('responseType', 'blob' as 'json');
    let httpParams = new HttpParams()
                      ;
    return this.http.get<multimedia>('http://localhost:3000/img/getall',
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }
}



