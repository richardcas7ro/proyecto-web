import { TestBed } from '@angular/core/testing';

import { ImageCompService } from './image-comp.service';

describe('ImageCompService', () => {
  let service: ImageCompService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImageCompService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
