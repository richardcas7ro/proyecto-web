import { Injectable } from '@angular/core';
import { Http } from '@angular/http'
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Image {

  constructor(private http: Http) {}

  public uploadImage(image: File): Observable<any> {
    var formData = new FormData();

    formData.append('image', image);

    return this.http.post('http://localhost:3000/img/createimg', formData);
  }
}
