import { TestBed } from '@angular/core/testing';

import { RolPerfilService } from './rol-perfil.service';

describe('RolPerfilService', () => {
  let service: RolPerfilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RolPerfilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
