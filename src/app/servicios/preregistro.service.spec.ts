import { TestBed } from '@angular/core/testing';

import { PreregistroService } from './preregistro.service';

describe('PreregistroService', () => {
  let service: PreregistroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreregistroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
