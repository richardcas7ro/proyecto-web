import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http'; 
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {RequestOptions, Request, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root',
})
export class PreregistroService  {
  [x: string]: any;

 url = environment.url;
 url2 = environment.url2;

  constructor(private http: HttpClient) { }

   // Http Options
   httpOptions = {
    
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*',
      'Authorization':'authkey',
      'userid':'1',
    }),
    //withCredentials: true
  }  ;
  

  postallpre(information){
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
                         .set('userid', '1');
    let httpParams = new HttpParams()
                        .set('username', '')
                        .set('password', '');
                     //   let httpBody = new httpBody().set
    return this.http.post('http://localhost:3000/pre/createPreUser',
    JSON.stringify(information),this.httpOptions);
    
    
     // this.url+'createPreUser'
      
     // {
     // headers: httpHeaders,
     // params: httpParams, 
     // responseType: 'json'
//);
  }
  getallpre(){
    return this.http.get(this.url+'findall');
  }

  getrol(nombrerol){
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
                         .set('userid', '1');
    let httpParams = new HttpParams()
                        .set('nombreRol', nombrerol)    
    return this.http.get('http://localhost:3000/rol/selectRol',
    {
       headers: httpHeaders,
       params: httpParams, 
       responseType: 'json'
    });
  }
  
  createuser(PreUsuario){

    var sixmon = new Date(PreUsuario.fechaIni); 
    sixmon.setMonth(sixmon.getMonth() + 6); 
    console.log('pre reg');
    
    const newUserObject = {      
      nombreUsuario:PreUsuario.idUsuario,
      contrasena:PreUsuario.contrasena,
      nombres:PreUsuario.nombres,
      apellidos:PreUsuario.apellidos,
      correo:PreUsuario.correo,
      activo:true,
      token: "",
      fechaIni:PreUsuario.fechaIni,
      fechafin:sixmon,
      idRol: PreUsuario.rol
  }
  console.log('......')
  console.log(PreUsuario)
  console.log(newUserObject)

    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
                         .set('userid', '1');
    let httpParams = new HttpParams()                      
    return this.http.post('http://localhost:3000/user/createUser',(newUserObject),
    {
       headers: httpHeaders,
       params: httpParams, 
       responseType: 'json'
    });
  };

  deleteuser(idPreUsuario){
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
                         .set('userid', '1');
    let httpParams = new HttpParams()
                        .set('idPreUsuario', idPreUsuario)
    return this.http.delete(this.url2+'delete',
    {
       headers: httpHeaders,
       params: httpParams, 
       responseType: 'json'
    });
  };

  createlink(event){

    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams();
                     //   let httpBody = new httpBody().set
                     return this.http.post('http://localhost:3000/user/createUserP',
                     event,
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }

}
