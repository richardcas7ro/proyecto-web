
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpParams } from '@angular/common/http'; 
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import {RequestOptions, Request, Headers } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class EventoService {

  constructor(private http: HttpClient) { }

  createEvent(event){

    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams();
                     //   let httpBody = new httpBody().set
                     return this.http.post('http://localhost:3000/event/create',
                     event,
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }

  
  geteve(event){
    console.log("ss");
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams()
                         .set('nombre', event);
    return this.http.get('http://localhost:3000/event/get',
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }


  getevell(){
    console.log("ss");
    let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json')
                         .set('Access-Control-Allow-Origin', '*')
                         .set('Authorization', 'authkey')
    let httpParams = new HttpParams()
                        ;
    return this.http.get('http://localhost:3000/event/getall',
                     {
                        headers: httpHeaders,
                        params: httpParams, 
                        responseType: 'json'
                     });
  }

}

