import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCompService } from '../servicios/image-comp.service';
import { first } from 'rxjs/operators';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder,  Validators } from '@angular/forms';
import { CommonModule }  from '@angular/common';
import { NgModule }      from '@angular/core';
import { FormGroup , FormControl , ReactiveFormsModule , FormsModule } from '@angular/forms';
import { AppComponent } from '../app.component';
import { EventoService } from '../servicios/evento.service';
import { PosterService } from '../services/poster.service';
import { evento } from '../modils/evento.models';
import {usuario} from '../Modils/Usuario.model';
import { posters } from '../Modils/posters.models';


interface Food {
  value: string;
  viewValue: string;
  meta: posters;
}

class ImageSnippet {

  pending: boolean = false;
  status: string = 'init'; 
 

  constructor(
   public src: string, public file: File
    ) {}
}


@Component({
  selector: 'app-cargar',
  templateUrl: './cargar.component.html',
  styleUrls: ['./cargar.component.css']
})

export class CargarComponent implements OnInit {
  foods: Food[] = new Array() ;
  selectedValue: posters;
  currentUser:usuario;
  selectedCar: string;
  listausuarios=[];
  com:String;
   selectedFile: ImageSnippet;
   loginForm3: FormGroup;
   @ViewChild(AppComponent) headerComponent : AppComponent;
  
  constructor(
    private PosterService: PosterService,
    private imageService: ImageCompService,
    private router: Router,
    private route: ActivatedRoute,
    private EventoService: EventoService,
    private formBuilder: FormBuilder
    ) { 
      this.currentUser = JSON.parse(localStorage.getItem('usuario'));
      this.com = localStorage.getItem('path');
    };

    

  ngOnInit(): void {
    this.loginForm3 = this.formBuilder.group({
      nombre:  ['', Validators.required],
      descripcion: ['', Validators.required],
    });
    
    this.PosterService.getevell(this.currentUser.idUser).pipe(first())
    .subscribe(
        data => {  
          console.log(data);
          const listausuarios = data;
          for(var i = 0; i < Object.values(data).length; i++){
            var datas:posters = data[i];
            var dd:Food = {value: '', viewValue: '', meta: datas}; 
            
            dd.value =  datas.titulo; 
            dd.viewValue =  datas.titulo + "-"+ datas.descripcion; 
            dd.meta = datas;
            this.foods.push(dd);
          }
        },
        error => { 
          console.log("error");
          alert("Error, No exiten eventos en el sistema.");        
        });
  }

  get f() { return this.loginForm3.controls; }

  processFile(imageInput: any) {
    
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    var ss =  this.loginForm3.controls;

    reader.addEventListener('load', (event: any) => {

      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.selectedFile.pending = true;
      console.log(this.selectedFile.file);
      this.imageService.uploadImage(this.selectedFile.file,this.selectedFile.src, ss.nombre.value, ss.descripcion.value, this.selectedValue ).subscribe(
        (res) => {
          this.onSuccess();
        },
        (err) => {
          this.onError();
        })
       } 
    );

    reader.readAsDataURL(file);
  }

  private onSuccess() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'ok';
  }
  
  private onError() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'fail';
    this.selectedFile.src = '';
  }

  onSubmit(){
  }

}
