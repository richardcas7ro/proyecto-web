import { Component, OnInit } from '@angular/core';
import {usuario} from '../Modils/Usuario.model';
import {evento} from '../Modils/evento.models';
import {posters} from '../Modils/posters.models';
import { Login } from '../servicios/login.service';
import { EventoService } from '../servicios/evento.service';
import { PosterService } from '../services/poster.service';
import { PreregistroService } from '../servicios/preregistro.service';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder,  Validators } from '@angular/forms';
import { CommonModule }  from '@angular/common';
import { first } from 'rxjs/operators';
import { NgModule }      from '@angular/core';
import { FormGroup , FormControl , ReactiveFormsModule , FormsModule } from '@angular/forms';
import { CDK_CONNECTED_OVERLAY_SCROLL_STRATEGY } from '@angular/cdk/overlay/overlay-directives';

interface Food {
  value: string;
  viewValue: string;
  meta: evento;
}

@Component({
  selector: 'app-posters',
  templateUrl: './posters.component.html',
  styleUrls: ['./posters.component.css']
})
export class PostersComponent implements OnInit {
 foods: Food[] = new Array() ;
 selectedValue: evento;
  loginForm4: FormGroup;
  currentUser:usuario;
  com:String;
  usuarios: usuario;
  eventos: evento;
  logUs:usuario;
  listaEventos=[];
  returnUrl: string;
  submitted = false;
  loading = false;
  editedmsg: string;
  constructor(private login:Login,
    private router: Router,
    private route: ActivatedRoute,
    private EventoService:EventoService,
    private PreregistroService: PreregistroService,
    private formBuilder: FormBuilder,
    private PosterService: PosterService) { 
      this.currentUser = JSON.parse(localStorage.getItem('usuario'));
      this.com = localStorage.getItem('path');
    }

  ngOnInit(): void {
    this.usuarios = new usuario;
    localStorage.setItem('path', 'login');
    this.loginForm4 = this.formBuilder.group({
        titulo:  ['', Validators.required],
        descripcion: ['', Validators.required],
        materia: ['', Validators.required],
        carrera: ['', Validators.required],
        grupo: ['', Validators.required],
        fechaIni: ['', Validators.required],
        fechafin: ['', Validators.required],
    });

    this.EventoService.getevell().pipe(first())
      .subscribe(
          data => {  
            
            const listausuarios = data;
            for(var i = 0; i < Object.values(data).length; i++){             
              var datas: evento = data[i];
              var dd:Food = {value: '', viewValue: '',meta:datas}; 
              dd.value =  datas.nombre; 
              dd.viewValue =  datas.nombre + "-"+ datas.descripcion; 
              dd.meta = data[i];
              this.foods.push(dd);
            }
          },
          error => { 
            console.log("error");
            alert("Error, No exiten eventos en el sistema.");        
    });
}

get f() { return this.loginForm4.controls; }

onSubmit() {
  
  var entrada = this.loginForm4.controls;
  const event : evento = (this.selectedValue);
  
  const newUserObject = {      
    titulo:entrada.titulo.value,
    descripcion:entrada.descripcion.value,
    materia:entrada.materia.value,
    carrera:entrada.carrera.value,
    grupo:entrada.grupo.value,
    statusAprob:false,
    vistas:0,
    fechaIni: event.fechaIni,
    fechafin:event.fechafin,
  } 
  
  this.PosterService.geteve(newUserObject.titulo).pipe(first())
  .subscribe(
      data => {         
       
          alert("Error, No se puede crear el Poster en el sistema.  Ya existe ese nombre");
      },
      error => { 

        var posterIdTemp ;
        
        this.PosterService.createEvent(newUserObject) // crea el poster
        .pipe(first())
        .subscribe(
            data => {
              console.log("poster exitoso");
              const newuniObject = {      
                idPoster: data.idPoster,
                idEvento: event.idEvent,
                fechaIni: event.fechaIni,
                fechafin:event.fechafin,
              } 
              posterIdTemp =  data.idPoster;
              this.PosterService.createlink(newuniObject) // lo relaciona la evento
        .pipe(first())
        .subscribe(
            data => {},
            error => {});

            const newlinkObject = {      
              idPoster: posterIdTemp,
              idUsuario: this.currentUser.idUser,
              idAprobadores: 0,
              fechaIni: event.fechaIni,
              fechafin:event.fechafin,
            } 

            this.PreregistroService.createlink(newlinkObject) // lo relaciona al usuario
        .pipe(first())
        .subscribe(
            data => {},
            error => {});

            alert("Generado, Poster creado en el sistema");
            this.reloadPage();
            },
            error => {
              console.log(error);
              alert("Error, No se puede crear el Poster en el sistema")
            });
        
      });
    }
    
    reloadPage() {
      window.location.reload();
    }

}
