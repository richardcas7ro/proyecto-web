import { Component, OnInit } from '@angular/core';
import {usuario} from './Modils/Usuario.model';
import { RandomService } from './servicios/random.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentUser:usuario;
  nologin = false;
  title = 'ProyectoAfiches';
  com:String;
  message:String;

  constructor(private someserv: RandomService){
    
    this.currentUser = JSON.parse(localStorage.getItem('usuario'));
    this.com = localStorage.getItem('path');
    
    if(this.com != null){
      if ( this.com.length > 1 ){ 
        this.nologin = this.com == 'login'? false : true;
      }
    }
  }

  ngOnInit(): void {
    /*
    this.someserv.telecast.subscribe(message => this.message = message);
    this.com = this.message;
    
    if(this.com != null){
      if ( this.com.length > 1 ){ 
        this.nologin = this.com == 'login'? false : true;
      }
    }*/
  }

  reloadPage() {
    window.location.reload();
/*
    this.someserv.telecast.subscribe(message => this.message = message);
    this.com = this.message;
    
    if(this.com != null){
      if ( this.com.length > 1 ){ 
        this.nologin = this.com == 'login'? false : true;
      }
    }*/
  }

}
